---
title: "Plaas 🐍📈"
date: 2018-12-13 13:25
tag:
- Python
- Falcon
- matplotlib
- API
- SaaS
- Docker
- Website
- XKCD-style
description: "Plots as a Service (XKCD style)"
externalLink: "https://github.com/biosan/plaas"
---
