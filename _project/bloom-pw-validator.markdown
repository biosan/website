---
title: "JS Bloom Filter Password Validator 🔐"
date: 2019-01-10 19:15
tag:
- Javascript
- Bloom Filter
- Password Validator
description: "JS Password Validator using a Bloom Filter"
externalLink: "https://github.com/biosan/bloom_pw_validator_js"
---
