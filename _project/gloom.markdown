---
title: "Gloom 🎲"
date: 2018-12-21 19:47
tag:
- Go
- Bloom Filter
- API
- Docker
description: "Go Bloom Filter (with REST API)"
externalLink: "https://github.com/biosan/gloom"
---
