---
title: "My Dotfiles"
date: 2018-10-24 12:34
tag:
- dotfiles
- macOS
- vim
- git
- shell
description: My very own dotfiles for macOS, they will tell you a lot about me
externalLink: https://github.com/biosan/dotfiles
---
