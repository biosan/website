---
title: About
layout: page
a: I'm always playing around with \*NIX systems, Docker, ZFS, cryptography stuff (PGP/GPG, OpenSSL, etc.)
---

---

Hello, I'm Alessandro Biondi, a software engineering undergraduate student at Sapienza University in Rome. 👨‍🎓

I'm a developer in love with Python, an aspiring photographer and cinema lover. *(Quentin & Stanley 😎)*

I'm a bit paranoid about privacy, security and data integrity, and that's probably why I'm passionte about cryptography, next-gen filesystems like ZFS and Btrfs, and everything that protect my data from evil algorithms, other people or very angry cosmic rays... 🔐

My mottos are: *"Encrypt all the things!"*, *"Backup all the things!"*

...also rember that *"Crypto means cryptography!"*


On the tech side, I'm a CLI enthusiast, a Mac guy and a Neovim happy user, make sure check my dotfiles if you whant to know more about my favourite tools and how I use them.

I also have some hardware influence, I started programming on embedded systems and I still have a thing for microcontrollers and electronics stuff.


I'm currently learning Ruby (and Rails) to get into something completely different from Python, and also Javascript to improve my front end skills.

Talking about my views on software development and design, I always search for better organization and design in software projects, 
I care a lot about correct implementation and good design, I’m a perfectionist, and I like to understand why things do or do not work.

I like to follow best practices and rules instead of *“be creative”* or *“make it work fast”*, but I’m not scared to do something new if there is no other way to solve a problem (that’s very unusual).

Last but not least, you have to know that I'm a organization and planning maniac... I try very hard to write, document, sort and plan almost everything in my life.

---

## Skills

- **Good at**
    - Python
    - C
    - Git
    - *NIX systems
<br>
<br>
- **A bit less good at**
    - Ruby (and Rails)
    - Java
    - Javascript
    - Docker

---


## Crypto stuff

My PGP/GPG Key fingerprint:


{% assign parts = site.pgp_fingerprint | split: ' ' %}

<blockquote>
<code>
{{ parts[0] }} {{ parts[1] }} {{ parts[2] }} {{ parts[3] }}<br>
 {{ parts[4] }} <strong>{{ parts[5] }} {{ parts[6] }} {{ parts[7] }}</strong>
 </code>
</blockquote>

If you send me anything using PGP/GPG make sure fingerprints match. You can also use [Keybase](https://keybase.io/{{ site.keybase }}) to verify it.

